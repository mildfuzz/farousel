(function(){
$.fn.farousel = function(method, options){
	"use strict";
	/*
	Accepts a string to directly call a method, or an object of options.
	
	Options:
	  height: int
	  width: int
	  slideDuration: int (this is per slide changed, so will be twice as long for two slides as it will for one)
	  flicker: selector for a button that changes slide position (must also have date-changeto attribute)
				  data-changeto can contain
				    n(int): will change to slide n
				    end(string) : will go to last slide
					start(string): goto first slide
				    up: will go next slide (add int after n to go up any number)
				    down: as above, but will come back, still accepts number to change
				  
	  methodOption: any type, will be passed to method as defined in first arg
	  css: 	object; this overrides any of the inbuild CSS settings.
			- structure -> css{slider{},container{},slides{}}
	
	
	HTML Structure:
		slider -> 
			sliderContainer -> 
				slide
				slide
				slide
		
	- slider must be passed to plugin ( $(slider).farousel(options) )
	
	- slider needs to either have explicit height, or be called after window load (if containing images)
	
	
	Events (all events output the slider object and current slide unless overridden)
	------
	-farouselStart , 
	-farouselEnd ,
	-farouselFail (contains message)
	-farouselFail (useful for debugging, outputs window.farousel object whenever it is affected by the code)
	
	---Farousel can also be activated via an event
	  $.event.trigger("farouselChangeto", changeto);
	
	  (changeto is required, and can be any value also accepted on a flicker)
	
	Window Level Global Flags
	-------------------------
	window.farousel.animating (boolean) : If the slider is operating or not
	
	Flags
	-----
	skipCheckWidth: Bool; Skips function for automatically checking container width and resizing.
	*/
	
	if(typeof(method) !== "string" && typeof(method) !== "undefined"){
		if (typeof(method) === "object" && typeof(options) !== "object"){
			options = method;
		} else {
			return false;
		}
	}
	
	if (typeof(options) !== "object" && typeof(options) !== "undefined"){
		return false;
	}
	
	//set global vars
	var slider = this;
	slider.container = $(this).children();
	
	slider.slides = $(this).children().eq(0).children();
	slider.count = $(this).children().eq(0).children().length;
	slider.current = 0;
	slider.flicker = $((options && options.hasOwnProperty('flicker') ? options.flicker : ".flicker"));
	
	
	var methods = {};
	methods.init = function(){
		if(!window.farousel){
			window.farousel = {};
		}
		methods.setWidth();
		methods.setHeight();
		methods.setSlideDuration();
		methods.setCSS();
		methods.checkContainerWidth();
		methods.trigger('init',{slides:slider.slides,current:slider.current});
	};
	methods.setCSS = function(){
		slider.css = {
			slider:{
				position: "relative",
				overflow:"hidden",
				width:slider.width+"px",
				height: slider.height+"px"
			},
			container:{
				position: "absolute",
				left:(slider.width*slider.current)+'px',
				width: slider.container.width+"px"
			},
			slides:{
				
				"float": "left",
				width: slider.width+"px"
			}
		};
		
		//merge objects
		
		
		if(options && typeof(options.css) === "object"){ 
			for(var element in slider.css){
				if(options.css[element]) {$.extend(slider.css[element], options.css[element]);} 
			}
		};
		console.log([slider.slides,slider.css]);
		
		$(slider).css( slider.css.slider );
		
		$(slider.container).css( slider.css.container );
		
		$(slider.slides).css( slider.css.slides );
	};

	methods.changeSlide = function(x){
		if(typeof(x) !== "number"){ x = 0;}
		methods.trigger('start');
		methods.setGlobalFlags('animating');
		slider.container.animate({"margin-left":(slider.width*-1)*x+"px"}, slider.slideDuration*slider.change, function(){
			methods.trigger('end');
			methods.setGlobalFlags('animating', false);
		});
	};
	methods.setWidth = function(){
			//set width of slider
			if(options && options.hasOwnProperty('width')){
				slider.width = options.width;
			} else {
				slider.width = $(slider.slides).eq(1).width();
			}
			slider.container.width = slider.width*slider.count;
			
	};
	methods.setHeight = function(){
			//set width of slider
			if(options && options.hasOwnProperty('height')){
				slider.height = options.height;
			} else {
				slider.height = $(slider.slides).eq(1).height();
			}
			
	};
	methods.setSlideDuration = function(){
			//set width of slider
			if(options && options.hasOwnProperty('slideDuration')){
				slider.slideDuration = options.slideDuration;
			} else {
				slider.slideDuration = 2000;
			}
			
	};
	methods.setStringFlicker = function(x){
		//removes number from end of string
		var change = {
			num: /\d+/.exec(x),
			dir: /[a-zA-Z]+/.exec(x)[0]
		};
		
		if(change.num){
			change.num = Number(change.num[0]);
		} else {
			change.num = 1;
		}
		return change;
		
	};
	methods.setCurrent = function(x){
		
		var newSlide;
		if(typeof(x) === "string"){
			var change = methods.setStringFlicker(x);
			
			switch (change.dir){
				case "up":
					newSlide = (slider.current + change.num > slider.count-1 ? slider.count-1 : slider.current + change.num);
					break;
				case "down":
					newSlide = (slider.current - change.num < 0 ? 0 : slider.current - change.num);
					break;
				case "start":
					newSlide = 0;
					break;
				case "end":
					newSlide = slider.count-1;
					break;
			}
			
		} else {
			//numeric change (none relative)
			newSlide = x;
		}
		if(newSlide === slider.current){
			methods.trigger('fail', "New Slide = "+newSlide+", Current Slide = "+slider.current);
			return false;
		} else {
			slider.change = Math.abs(slider.current-newSlide);
			slider.current = newSlide;
			return true;
		}
	};
	methods.trigger = function(x, message){
		if(typeof(message) === "undefined"){ message = {slider:slider,current: slider.current};}
		var trig;
		switch(x){
			case "init":
				trig = "farouselInit";
				break;
			case "fail":
				trig = "farouselFailed";
				break;
			case "start":
				trig = "farouselStart";
				break;
			case "end":
				trig = "farouselEnd";
				break;
			case "flags":
				trig = "farouselFlags";
				break;
		}
		$.event.trigger(trig, message);
	};
	methods.checkEffectsQueue = function(){
		if (slider.container.queue('fx').length){
			methods.trigger('fail',"FX queue is running");
			return false;
		} else {
			return true;
		}
	};
	methods.runChecksBeforeChangeSlide = function(data){
		//check to see if slide actually needs changing before launching animation. Prevents stillborn animations, also ensures queue is empty
		if(methods.checkEffectsQueue() && methods.setCurrent(data)){
			methods.changeSlide(slider.current);
		}
	};
	methods.setGlobalFlags = function(flag, value){
		if(typeof(value) === "undefined") {value = true;}
		window.farousel[flag] = value;
		methods.trigger('flags',window.farousel); 
	};
	methods.checkContainerWidth = function(){
		if(!options.skipCheckWidth){
			while(slider.container.height() > slider.height){

				slider.container.css({width:($(slider.container).width()+1)+"px"});
			}
		} 
		
	};
	
	if(typeof(method) === "string"){
		methods[method]((function(){
			if(option && options.methodOption){
				return options.methodOption;
			}
			}));
			return false;
	}
	
	$(slider.flicker).live('click',function(){
			var clicked = $(this);
			methods.runChecksBeforeChangeSlide(clicked.data("changeto"));
	});
	
	$(document).bind('farouselChangeto', function(x, data ){
		
		methods.runChecksBeforeChangeSlide(data);
	});
	
	methods.init();
	
	
};
})(jQuery);

